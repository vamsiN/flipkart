import { Component } from 'react'

import HeaderTest from './Components/HeaderTest/HeaderTest';

import Header from './Components/Header/Header';
import MenuSection from './Components/MenuSection/MenuSection';
import BannerSection from './Components/BannerSection/BannerSection';
import ElectronicsItem from './Components/ElectronicsItems/ElectronicsItems';
import MonthlyEssentials from './Components/MonthlyEssentials/MonthlyEssentials';
import HomeKitchenSection from './Components/HomeKitchenSection/HomeKitchenSection';
import TopDeals from './Components/TopDeals/TopDeals'
import Footer from './Components/Footer/Footer';
import AllProductsSection from './Components/AllProductsSection/AllProductsSection';

class App extends Component {



  render() {
    return (
      <div className='container-fluid bg-light ' >

        <HeaderTest />
        <ElectronicsItem />
        <MonthlyEssentials />
        <HomeKitchenSection />
        <TopDeals /> 
        {/* <Footer /> */}
        {/* <Header />


        {/* <MenuSection />
        <BannerSection /> */}
        {/* <ElectronicsItem />
        <MonthlyEssentials />
        <HomeKitchenSection />
        
        <Footer />
         */}

      </div>
    );
  }

}

export default App;
