
// import ProductCard from './Components/ProductCard';
import { Component } from 'react'
// import axios from 'axios'
// import './Components/Loader'
// import Loader from './Components/Loader';
// import Error from './Components/Error.js';

import ProductCard from '../ProductCard/ProductCard'
import Error from '../Error/Error'
import Loader from '../Loader/Loader'
import axios from 'axios'




class AllProductsSection extends Component {


  state = {
    productsList: [],
    status: 'loading',


  }

  componentDidMount = () => {

    axios.get('https://fakestoreapi.com/products')
      .then((response) => {
        console.log(response)
        this.setState({
          productsList: response.data,
          status: 'loaded'
        })
      })
      .catch((error) => {
        console.log(error)
        this.setState({
          status: 'error',

        })
      })

  }


  render() {
    const { productsList, status } = this.state
    console.log(productsList)
    return (
      <>

        {status === 'loading' ?
          <Loader />
          :
          undefined
        }

        {status === 'error' ?
          < Error />
          :
          undefined
        }

        {status === 'loaded' ?

          <>
            <div>
              <div className='container '>

                <div className='row justify-content-center m-3'>
                  {productsList.map((eachProduct) => (
                    < ProductCard eachProductDetails={eachProduct} key={eachProduct.id} />
                  ))}

                </div>
              </div>

            </div>
          </>


          :
          undefined
        }







      </>
    );
  }

}

export default AllProductsSection;
