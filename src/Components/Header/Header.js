import { BsSearch } from 'react-icons/bs'
import { FaShoppingCart } from 'react-icons/fa'

import { MdNotifications } from 'react-icons/md'
import { RiQuestionnaireFill } from 'react-icons/ri'
import { IoIosTrendingUp } from 'react-icons/io'
import { HiDownload } from 'react-icons/hi'

const Header = () => {
    return (
        <div className='row  pt-1 d-flex  justify-content-center' >
            <div className='col-2 flex-shrink-1 d-flex flex-column align-items-center justify-content-center   '>
                <div className='col-10  text-center'>
                    <img height='25'
                        src="//img1a.flixcart.com/www/linchpin/fk-cp-zion/img/flipkart-plus_8d85f4.png"
                        alt="flipkart-logo"
                    />
                </div>
                <div className='col-10 text-center  '>
                    <span className='' style={{ color: 'white'}}>Explore Plus</span>
                    <span>
                        <img width='15' src='//img1a.flixcart.com/www/linchpin/fk-cp-zion/img/plus_aef861.png' alt='explore plus' />
                    </span>
                </div>
            </div>


            <div className='col-3 flex-shrink-1 d-flex flex-column justify-content-center  '>
                <div className="input-group">
                    <input type="text" className="form-control flex-shrink-1" style={{ borderRightWidth: '0px', borderRadius:'0px' }} placeholder="Search for products brands and more" />
                    <span class="input-group-text flex-shrink-1" style={{ backgroundColor: 'white', borderLeftWidth: '0px', borderRadius:'0px' }} id="basic-addon2"><BsSearch /></span>
                </div>
            </div>



            <div className='col-1 flex-shrink-1   d-flex flex-column justify-content-center'>
                {/* <button className='btn btn-light text-primary w-100 mt-3 ps-3 pe-3'>Login</button> */}
                <div className="dropdown-center   ">
                    <button className="btn btn-btn-light text-primary bg-white " style={{  borderRadius:'0px' }} type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Login
                    </button>
                    <ul class="dropdown-menu">
                        <li><a className="dropdown-item" href="#">New Customer <span className='text-primary'>Sign up</span></a></li>
                        <hr />
                        <li><a className="dropdown-item" href="#">My Profile</a></li>
                        <hr />
                        <li><a className="dropdown-item" href="#">Orders</a></li>
                        <hr />
                        <li><a className="dropdown-item" href="#">Whislist</a></li>
                        <hr />
                        <li><a className="dropdown-item" href="#">Rewards</a></li>
                        <hr />
                        <li><a className="dropdown-item" href="#">Gift Cards</a></li>
                    </ul>
                </div>
            </div>


            <div className='col-2 flex-shrink-1  d-flex flex-column justify-content-center'>
                <p className='text-light text-center m-2    '>Become a Seller</p>
            </div>


            <div className='col-1 flex-shrink-1 d-flex flex-column justify-content-center  '>

                <div className="dropdown align-self-center  ">
                    <button className="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        More
                    </button>
                    <ul className="dropdown-menu">
                        <li><a className="dropdown-item" href="#">< MdNotifications />  Notifications</a></li>
                        <hr />
                        <li><a className="dropdown-item" href="#">< RiQuestionnaireFill />  24x7 Customer Care</a></li>
                        <hr />
                        <li><a className="dropdown-item" href="#">< IoIosTrendingUp />  Advertise</a></li>
                        <hr />
                        <li><a className="dropdown-item" href="#">< HiDownload />  Download App</a></li>
                    </ul>
                </div>
            </div>
            <div className='col-1 flex-shrink-1   d-flex flex-column justify-content-center'>
                <div>
                    <span className='   flex-grow-1 text-white m-1 '><FaShoppingCart /></span><span className='text-light   flex-grow-1 m2'>Cart</span>
                </div>
            </div>
        </div>

    )
}

export default Header