

import { BsSearch } from 'react-icons/bs'
import { FaShoppingCart } from 'react-icons/fa'

import { MdNotifications } from 'react-icons/md'
import { RiQuestionnaireFill } from 'react-icons/ri'
import { IoIosTrendingUp } from 'react-icons/io'
import { HiDownload } from 'react-icons/hi'

const HeaderTest = () => {

    return (
        <>
            <div className='row  bg-primary'>
                <div className='col-10 ps-0 m-auto text-center d-flex align-items-center '>

                    <div className='col-2 d-flex flex-column  justify-content-center '>
                        {/* <h1 className=' p-0 m-0 border border-success' style={{ fontSize: "6vw" }}>Flipkart</h1> */}
                        <div className='col-10   text-center'>
                            <img height='30' width='120' className='img-fluid'
                                src="//img1a.flixcart.com/www/linchpin/fk-cp-zion/img/flipkart-plus_8d85f4.png"
                                alt="flipkart-logo"
                            />
                            {/* <h1 className='text-white p-0 m-0 ' style={{ fontSize: "4vw" }}>Flipkarts</h1> */}
                        </div>
                        <div className='col-10  text-center  '>
                            <p className=' text-white p-0 m-0' style={{ fontSize: "1vw", fontStyle: 'italic' }}>Explore Pluss</p>
                            {/* <span>
                                <img className='m-1' width='15vw' src='//img1a.flixcart.com/www/linchpin/fk-cp-zion/img/plus_aef861.png' alt='explore plus' />
                            </span> */}
                        </div>
                    </div>


                    <div className='col-4 p-2   d-flex '>
                        {/* <h1 className=' border border-success p-0 m-0' style={{ fontSize: "4vw" }}>header</h1> */}
                        <div className="input-group flex-">
                            <input type="text" className="form-control flex-shrink-1" style={{ borderRightWidth: '0px', borderRadius: '0px', width: '16vw', height: '2vw' }} placeholder="" />
                            {/* <span class="input-group-text flex-shrink-1" style={{ backgroundColor: 'white', borderLeftWidth: '0px', borderRadius: '0px' }} id="basic-addon2"><BsSearch /></span> */}
                        </div>
                    </div>


                    <div className='text-right col-1   align-items-center' style={{}}>
                        {/* <h1 className=' p-0 m-0 border border-success' style={{ fontSize: "4vw" }}>Flipkart</h1> */}
                        <div className="dropdown-center  ">
                            <button className="btn btn-btn-light btn-sm text-primary bg-white text-center" style={{ borderRadius: '0px', fontSize: "1vw", width: '5vw', height: '2vw' }} type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                Login
                            </button>
                            <ul class="dropdown-menu">
                                <li><a className="dropdown-item" href="#">New Customer <span className='text-primary'>Sign up</span></a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">My Profile</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">Orders</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">Whislist</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">Rewards</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">Gift Cards</a></li>
                            </ul>
                        </div>
                    </div>


                    <div className='col-2 flex-shrink-1  d-flex flex-column justify-content-center' style={{fontSize: "1vw"}}>
                        <p className='text-light text-center m-2  '>Become a Seller</p>
                    </div>


                    <div className='col-1 flex-shrink-1   d-flex flex-column justify-content-center '>

                        <div className="dropdown align-self-center  ">
                            <button className="btn btn-primary dropdown-toggle " type="button" data-bs-toggle="dropdown" aria-expanded="false"  style={{fontSize: "1vw"}}> 
                                More
                            </button>
                            <ul className="dropdown-menu" style={{fontSize: "2vw"}}>
                                <li><a className="dropdown-item" href="#">< MdNotifications />  Notifications</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">< RiQuestionnaireFill />  24x7 Customer Care</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">< IoIosTrendingUp />  Advertise</a></li>
                                <hr />
                                <li><a className="dropdown-item" href="#">< HiDownload />  Download App</a></li>
                            </ul>
                        </div>
                    </div>
                    <div className='col-1 flex-shrink-1   d-flex flex-column justify-content-center' style={{fontSize: "1vw"}}>
                        <div>
                            <span className='   flex-grow-1 text-white m-1 '><FaShoppingCart /></span><span className='text-light   flex-grow-1 m2'>Cart</span>
                        </div>
                    </div>



                </div>

            </div>

        </>








        // <div className='row bg-primary pt-1 d-flex  justify-content-center' >
        //     <div className='col-2 flex-shrink-1 d-flex flex-column align-items-center justify-content-center   '>
        // <div className='col-10  text-center'>
        //     <img height='25'
        //         src="//img1a.flixcart.com/www/linchpin/fk-cp-zion/img/flipkart-plus_8d85f4.png"
        //         alt="flipkart-logo"
        //     />
        // </div>
        // <div className='col-10 text-center  '>
        //     <span className='' style={{ color: 'white'}}>Explore Plus</span>
        //     <span>
        //         <img width='15' src='//img1a.flixcart.com/www/linchpin/fk-cp-zion/img/plus_aef861.png' alt='explore plus' />
        //     </span>
        // </div>
        //     </div>


        //     <div className='col-3 flex-shrink-1 d-flex flex-column justify-content-center  '>
        // <div className="input-group">
        //     <input type="text" className="form-control flex-shrink-1" style={{ borderRightWidth: '0px', borderRadius:'0px' }} placeholder="Search for products brands and more" />
        //     <span class="input-group-text flex-shrink-1" style={{ backgroundColor: 'white', borderLeftWidth: '0px', borderRadius:'0px' }} id="basic-addon2"><BsSearch /></span>
        // </div>
        //     </div>



        //     <div className='col-1 flex-shrink-1   d-flex flex-column justify-content-center'>
        //         {/* <button className='btn btn-light text-primary w-100 mt-3 ps-3 pe-3'>Login</button> */}
        // <div className="dropdown-center   ">
        //     <button className="btn btn-btn-light text-primary bg-white " style={{  borderRadius:'0px' }} type="button" data-bs-toggle="dropdown" aria-expanded="false">
        //         Login
        //     </button>
        //     <ul class="dropdown-menu">
        //         <li><a className="dropdown-item" href="#">New Customer <span className='text-primary'>Sign up</span></a></li>
        //         <hr />
        //         <li><a className="dropdown-item" href="#">My Profile</a></li>
        //         <hr />
        //         <li><a className="dropdown-item" href="#">Orders</a></li>
        //         <hr />
        //         <li><a className="dropdown-item" href="#">Whislist</a></li>
        //         <hr />
        //         <li><a className="dropdown-item" href="#">Rewards</a></li>
        //         <hr />
        //         <li><a className="dropdown-item" href="#">Gift Cards</a></li>
        //     </ul>
        // </div>
        //     </div>


        // <div className='col-2 flex-shrink-1  d-flex flex-column justify-content-center'>
        //     <p className='text-light text-center m-2    '>Become a Seller</p>
        // </div>


        // <div className='col-1 flex-shrink-1 d-flex flex-column justify-content-center  '>

        //     <div className="dropdown align-self-center  ">
        //         <button className="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
        //             More
        //         </button>
        //         <ul className="dropdown-menu">
        //             <li><a className="dropdown-item" href="#">< MdNotifications />  Notifications</a></li>
        //             <hr />
        //             <li><a className="dropdown-item" href="#">< RiQuestionnaireFill />  24x7 Customer Care</a></li>
        //             <hr />
        //             <li><a className="dropdown-item" href="#">< IoIosTrendingUp />  Advertise</a></li>
        //             <hr />
        //             <li><a className="dropdown-item" href="#">< HiDownload />  Download App</a></li>
        //         </ul>
        //     </div>
        // </div>
        // <div className='col-1 flex-shrink-1   d-flex flex-column justify-content-center'>
        //     <div>
        //         <span className='   flex-grow-1 text-white m-1 '><FaShoppingCart /></span><span className='text-light   flex-grow-1 m2'>Cart</span>
        //     </div>
        // </div>
        // </div>

    )
}

export default HeaderTest