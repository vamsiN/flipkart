const MonthlyEssentials = () => {

    const monthlyEssentialsItemsArray = [
        {   
            id:0,
            image: "https://rukminim1.flixcart.com/image/150/150/kwtkxow0/hair-oil/y/w/z/30-beard-oil-for-beard-hair-growth-and-moustache-for-men-with-21-original-imag9ewaqezncmng.jpeg?q=70",
            category: 'Beard Care',
            cost: 'Min. 20% Off',
            
        },
        {   
            id:1,
            image: "https://rukminim1.flixcart.com/image/150/150/xif0q/nut-dry-fruit/v/u/t/250-popular-california-1-pouch-farmley-original-imagh97wgkcq257h.jpeg?q=70",
            category: 'Food Products',
            cost: 'Min. 20% Off',
        },
        {   
            id:2,
            image: "https://rukminim1.flixcart.com/image/150/150/krqoknk0/torch/p/j/j/rechargeable-2-mode-700-meter-long-range-waterproof-led-outdoor-original-imag5gpy8zjvshyw.jpeg?q=70",
            category: 'Utility Lighting',
            cost: 'Min. 50% Off',
        },
        {   
            id:3,
            image: "https://rukminim1.flixcart.com/image/150/150/kvvad8w0/protein-supplement/l/o/q/whey-protein-beginner-s-nut5609-01-muscleblaze-original-imag8z587jxrjgpv.jpeg?q=70",
            category: 'Protien Suppliments',
            cost: 'Min. 30% Off',
        },
        {   
            id:4,
            image: "https://rukminim1.flixcart.com/image/150/150/ky1vl3k0/electric-insect-killer/8/q/0/pest-reject-device-non-toxic-spider-lizard-mice-repellent-indoor-original-imagadhhrbphzuzg.jpeg?q=70",
            category: 'Top Cameras',
            cost: 'Min. 50% Off',
        },
        {
            id:5,
            image: "https://rukminim1.flixcart.com/image/150/150/jy0frm80/deodorant/g/f/p/400-hamilton-deodorant-body-spray-denver-men-original-imafgb9fsafampwz.jpeg?q=70",
            category: 'Monitors',
            cost: 'Min. 30% Off',
        }       

    ]

    return (
        <div className='row mt-3   me-2 ms-2 mt-4 'style={{backgroundColor:'white'}}>
            <div className='col-11 m-auto pt-3 d-flex flex-row justify-content-between'>
                <div>
                <span className='text-dark' style={{ fontSize: '20px', fontWeight: 'bold', fontSize:'2vw' }}>Monthly Essentials</span>
                </div>
                <div>
                <button className=' btn btn-primary btn-sm' style={{ fontSize: '20px', fontWeight: 'bold', fontSize:'2vw'}}>VIEW ALL</button>
                </div>
            </div>

            <hr className='mt-3' />

            {monthlyEssentialsItemsArray.map((eachProduct) => {
                return (
                    <div className='col-4 col-md-3 col-lg-2  m-auto  '>
                        <div className='card col-10' style={{borderWidth:'0px', marginTop:'0px', fontSize:'2vw'}}> 
                            <img src={eachProduct.image} className="card-img-top p-2 p-md-3 mx-auto image-fluid " alt="product-image " style={{ borderRightWidth: '0px', borderRadius: '0px', width: '10vw', height: '10vw' }}/>
                            <div className="card-body card-body-sm  p-0 m-2" >
                                <h5 className="card-title text-center" style={{borderWidth:'0px', marginTop:'0px', fontSize:'1vw'}}>{eachProduct.category}</h5>
                                <p className="card-text text-center text-success "  style={{borderWidth:'0px', marginTop:'0px', fontSize:'1vw'}}>From {eachProduct.cost}</p>
                                {/* <p className="card-text text-center">{eachProduct.companey}</p> */}
                            </div>
                        </div>
                    </div>
                )
            })}


        </div>
    )
    
}

export default MonthlyEssentials