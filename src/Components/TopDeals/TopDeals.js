const TopDeals = () => {

    const topDealsArray = [
        {   
            id:0,
            image: "https://rukminim1.flixcart.com/flap/150/150/image/679c9ee1d28379f0.jpg?q=70",
            category: 'Mobile Back Covers',
            cost: 'From 99',
            info:'For All Top Models'
            
        },
        {   
            id:1,
            image: "https://rukminim1.flixcart.com/image/150/150/kbzergw0/instant-camera/5/c/m/instax-instant-camera-mini-11-fujifilm-original-imaft7fpmhyy5my7.jpeg?q=70",
            category: 'Instax Cameras',
            cost: 'From 3,999',
            info: 'Capture your Unique'
        },
        {   
            id:2,
            image: "https://rukminim1.flixcart.com/image/150/150/k6qsn0w0/tablet/3/q/z/lenovo-za5g0047in-original-imafp4w7qvpnprv5.jpeg?q=70",
            category: 'Lenovo Tab M8',
            cost: 'Shop Now!',
            info: '8-inch| Upto 64GB'
        },
        {   
            id:3,
            image: "https://rukminim1.flixcart.com/image/150/150/kzsqykw0/mobile-camera-lens-protector/p/g/v/yes-cp-12-zoy-original-imagbqghwbumu4qj.jpeg?q=70",
            category: 'Lens Protector',
            cost: 'from 99',
            info: 'For All Top Models'
        },
        {   
            id:4,
            image: "https://rukminim1.flixcart.com/image/150/150/ktop5e80/tablet/x/9/o/mk2k3hn-a-apple-original-imag6yy7xjjugz4w.jpeg?q=70",
            category: 'Apple iPads',
            cost: 'Shop Now',
            info:'Liquid Retina Display'
        },
        {
            id:5,
            image: "https://rukminim1.flixcart.com/image/150/150/ksxjs7k0/tablet/n/j/b/rmp2102-realme-original-imag6e3assqyqhfa.jpeg?q=70",
            category: 'realme Pad',
            cost: 'Shop Now',
            info:'#1 Best Seller In Charts'
        }       

    ]

    // return (
    //     <div className='row mt-3   me-5 ms-5 mt-4 mb-5' style={{backgroundColor:'white'}}>
    //         <div className='col-12 m-auto pt-3 d-flex flex-row justify-content-between'>
    //             <div>
    //             <span className='text-dark' style={{ fontSize: '20px', fontWeight: 'bold' }}>Top Deals</span>
    //             </div>
    //             <div>
    //             <button className=' btn btn-primary '>VIEW ALL</button>
    //             </div>
    //         </div>

    //         <hr className='mt-3' />

    //         {topDealsArray.map((eachProduct) => {
    //             return (
    //                 <div className='col-2 p-3 m-auto '>
    //                     <div className='card' style={{borderWidth:'0px', marginTop:'0px'}}> 
    //                         <img src={eachProduct.image} className="card-img-top p-5 " alt="product-image" />
    //                         <div className="card-body">
    //                             <h5 className="card-title text-center">{eachProduct.category}</h5>
    //                             <p className="card-text text-center text-success">From {eachProduct.cost}</p>
    //                             {/* <p className="card-text text-center">{eachProduct.companey}</p> */}
    //                         </div>
    //                     </div>
    //                 </div>
    //             )
    //         })}
    //     </div>
    // )

    return (
        <div className='row mt-3   me-2 ms-2 mt-4 'style={{backgroundColor:'white'}}>
            <div className='col-11 m-auto pt-3 d-flex flex-row justify-content-between'>
                <div>
                <span className='text-dark' style={{ fontSize: '20px', fontWeight: 'bold', fontSize:'2vw' }}>Top Deals</span>
                </div>
                <div>
                <button className=' btn btn-primary btn-sm' style={{ fontSize: '20px', fontWeight: 'bold', fontSize:'2vw'}}>VIEW ALL</button>
                </div>
            </div>

            <hr className='mt-3' />

            {topDealsArray.map((eachProduct) => {
                return (
                    <div className='col-4 col-md-3 col-lg-2  m-auto  '>
                        <div className='card col-10' style={{borderWidth:'0px', marginTop:'0px', fontSize:'2vw'}}> 
                            <img src={eachProduct.image} className="card-img-top p-2 p-md-3 mx-auto image-fluid " alt="product-image " style={{ borderRightWidth: '0px', borderRadius: '0px', width: '10vw', height: '10vw' }}/>
                            <div className="card-body card-body-sm  p-0 m-2" >
                                <h5 className="card-title text-center" style={{borderWidth:'0px', marginTop:'0px', fontSize:'1vw'}}>{eachProduct.category}</h5>
                                <p className="card-text text-center text-success "  style={{borderWidth:'0px', marginTop:'0px', fontSize:'1vw'}}>From {eachProduct.cost}</p>
                                {/* <p className="card-text text-center">{eachProduct.companey}</p> */}
                            </div>
                        </div>
                    </div>
                )
            })}


        </div>
    )

}

export default TopDeals