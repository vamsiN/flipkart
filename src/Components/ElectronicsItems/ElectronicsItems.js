import './ElectronicsItems.css'

const ElectronicsItem = () => {

    const electronicsItemsArray = [
        {
            id: 0,
            image: "https://rukminim1.flixcart.com/image/150/150/printer/j/j/y/hp-laserjet-m1005-multifunction-original-imadxhzpeb9qbrfg.jpeg?q=70",
            category: 'Printers',
            cost: '3999',
            companey: 'HP'
        },
        {
            id: 1,
            image: "https://rukminim1.flixcart.com/image/150/150/kyag87k0/computer/7/r/u/raider-ge66-12ugs-gaming-laptop-msi-original-imagakb9zw7gqzcm.jpeg?q=70",
            category: 'laptops',
            cost: '2336',
            companey: 'Canon'
        },
        {
            id: 2,
            image: "https://rukminim1.flixcart.com/image/150/150/kx50gi80/projector/r/9/p/zeb-pixa-play-12-5-6-dobly-audio-led-projector-zebronics-original-imag9z3yujqmzqt4.jpeg?q=70",
            category: 'Projectors',
            cost: '9999',
            companey: 'Zebronics'
        },
        {
            id: 3,
            image: "https://rukminim1.flixcart.com/image/150/150/ktop5e80/computer/f/p/w/km513ua-l503ts-thin-and-light-laptop-asus-original-imag6yt6ftegd5bx.jpeg?q=70",
            category: 'Core i5 Laptops',
            cost: '39,990',
            companey: 'HP, Asus, Dell & More'
        },
        {
            id: 4,
            image: "https://rukminim1.flixcart.com/image/150/150/l5jxt3k0/dslr-camera/m/n/a/-original-imagg7hsggshhwbz.jpeg?q=70",
            category: 'Top Cameras',
            cost: '42,000',
            companey: 'Canon, Sony, Fujifilm'
        },
        {
            id: 5,
            image: "https://rukminim1.flixcart.com/image/150/150/ky7lci80/monitor/z/7/d/-original-imagagyyben5cezf.jpeg?q=70",
            category: 'Monitors',
            cost: '8000',
            companey: 'Top Rated'
        }

    ]

    // return (
    //     <div className='row mt-3 section  me-5 ms-5 mb-3  border border-success' style={{ backgroundColor: 'white', width: "90%",  height: '25rem' }}>
    //         <div className='col-12 m-auto  d-flex flex-row justify-content-between flex-wrap ' style={{   height: '2rem' }}>
    //             <div>
    //                 <span className='text-dark' >Best of Electronics</span>
    //             </div>
    //             <div>
    //                 <button className=' btn btn-primary section '>VIEW ALL</button>
    //             </div>
    //         </div>

    //         <hr className='mt-3' />

    //         <div className='col-5 col-md-10 pt-3 d-flex flex-row flex-shrink justify-content-between border border-danger' style={{ width: "100%", height: '18rem' }}>
    //             {
    //                 electronicsItemsArray.map((eachProduct) => {
    //                     return (
    //                         <div className='col-2  border border-success contain flex-shrink' style={{  height: '15rem', }}>
    //                             {/* <div className='card  border border-danger ' style={{ borderWidth: '0px', marginTop: '0px', width: "100%", height:'100%'}}>
    //                             <img src={eachProduct.image} className="card-img-top m-auto " alt="product-image" style={{ width: "100%", height:'50%'}}/>
    //                             <div className="card-body" style={{ width: "100%", height:'100%'}}>
    //                                 <h5 className="card-title text-center">{eachProduct.category}</h5>
    //                                 <p className="card-text text-center text-success">From {eachProduct.cost}</p>
    //                                 <p className="card-text text-center">{eachProduct.companey}</p>
    //                             </div>
    //                         </div> */}
    //                             <div className=" col-12 card " style={{height: '15rem'}}>
    //                                 <img className="card-img-top m-auto" src={eachProduct.image} style={{ width: "60%", height:"10rem"}} alt="Card image cap" />
    //                                     <div className="card-body">
    //                                     <p className="card-title text-center">{eachProduct.category}</p>
    //                                 <p className="card-text text-center text-success">From {eachProduct.cost}</p>
    //                                 <p className="card-text text-center">{eachProduct.companey}</p>
    //                                     </div>
    //                             </div>
    //                         </div>
    //                     )
    //                 })
    //             }


    //         </div >



    //     </div>


    // )

    return (
        <div className='row mt-3   me-2 ms-2 mt-4 ' style={{ backgroundColor: 'white' }}>
            <div className='col-11 m-auto pt-3 d-flex flex-row justify-content-between '>
                <div>
                    <span className='text-dark' style={{ fontSize: '20px', fontWeight: 'bold', fontSize:'2vw' }}>Best of Electronics</span>
                </div>
                <div>
                    <button className=' btn btn-primary btn-sm' style={{ fontSize: '20px', fontWeight: 'bold', fontSize:'2vw'}}>VIEW ALL</button>
                </div>
            </div>

            <hr className='mt-3' />

            {electronicsItemsArray.map((eachProduct) => {
                return (
                    <div className='col-4 col-md-3 col-lg-2  m-auto '>
                        <div className='card col-10' style={{ borderWidth: '0px', marginTop: '0px', fontSize: '2vw' }}>
                            <img src={eachProduct.image} className="card-img-top p-2 p-md-3 mx-auto image-fluid " alt="product-image " style={{ borderRightWidth: '0px', borderRadius: '0px', width: '10vw', height: '10vw' }} />
                            <div className="card-body card-body-sm  p-0 m-2" >
                                <h5 className="card-title text-center" style={{ borderWidth: '0px', marginTop: '0px', fontSize: '1vw' }}>{eachProduct.cost}</h5>
                                <p className="card-text text-center text-success " style={{ borderWidth: '0px', marginTop: '0px', fontSize: '1vw' }}>From {eachProduct.companey}</p>
                                {/* <p className="card-text text-center">{eachProduct.companey}</p> */}
                            </div>
                        </div>
                    </div>
                )
            })}


        </div>
    )
}

export default ElectronicsItem