const HomeKitchenSection = () => {

    const homeKitchenSectionItemsArray = [
        {   
            id:0,
            image: "https://rukminim1.flixcart.com/image/150/150/kz8qsnk0/mosquito-net/h/1/p/double-bed-polyester-adults-blue-mosquito-net-double-bed-nets-original-imagbagfgnfvnquj.jpeg?q=70",
            category:'Mosquito Nets',
            cost: 'From 149',
            offer:'Best Deal Price'
            
        },
        {   
            id:1,
            image: "https://rukminim1.flixcart.com/image/150/150/kz065jk0/emergency-light/v/t/4/high-quality-60-high-bright-led-light-with-android-charging-original-imagb3v8h8zdm3q3.jpeg?q=70",
            category:'Emergency Lights',
            cost: 'Upto 80% off',
            offer:'Big Discounts'
        },
        {   
            id:2,
            image: "https://rukminim1.flixcart.com/image/150/150/k0e66q80/plant-sapling/c/e/7/air-purifying-pothos-money-plant-with-imported-pot-1-water-original-imafk747aczgq6ze.jpeg?q=70",
            category: 'Live Plants',
            cost:'From 199',
            offer: 'Best Selling',
        },
        {   
            id:3,
            image: "https://rukminim1.flixcart.com/image/150/150/k3j1z0w0/ceiling-lamp/w/7/r/gold-01-classical-original-imafmbywhhurgf6b.jpeg?q=70",
            category: 'Decor Lightings',
            cost: 'Upto 80% Off',
            offer: 'Big Discounts',
        },
        {   
            id:4,
            image: "https://rukminim1.flixcart.com/image/150/150/kod858w0/paint-roller/8/l/f/black-paint-tray-9-inch-painting-roller-orson-original-imag2u6esa4cxrb9.jpeg?q=70",
            category: 'Paint Rollers',
            cost: 'From 129',
            offer: 'Best Selling',
        },
        {
            id:5,
            image: "https://rukminim1.flixcart.com/image/150/150/l5iid8w0/curtain/c/w/j/blue-blossoms-213-door-fs2pc001353dr-eyelet-fashion-string-original-imagg68893hsxpak.jpeg?q=70",
            category: 'Curtains',
            cost: 'From 99',
            offer: 'Big Discounts',
        }       

    ]

    // return (
    //     <div className='row mt-3   me-5 ms-5 mt-4 mb-4' style={{backgroundColor:'white'}}>
    //         <div className='col-12 m-auto pt-3 d-flex flex-row justify-content-between'>
    //             <div>
    //             <span className='text-dark' style={{ fontSize: '20px', fontWeight: 'bold' }}>Home & Kitchen Essentials</span>
    //             </div>
    //             <div>
    //             <button className=' btn btn-primary '>VIEW ALL</button>
    //             </div>
    //         </div>

    //         <hr className='mt-3' />

    //         {homeKitchenSectionItemsArray.map((eachProduct) => {
    //             return (
    //                 <div className='col-2 p-3 m-auto '>
    //                     <div className='card' style={{borderWidth:'0px', marginTop:'0px'}}> 
    //                         <img src={eachProduct.image} className="card-img-top p-5 " alt="product-image" />
    //                         <div className="card-body">
    //                             <h5 className="card-title text-center">{eachProduct.category}</h5>
    //                             <p className="card-text text-center text-success">From {eachProduct.cost}</p>
    //                             {/* <p className="card-text text-center">{eachProduct.companey}</p> */}
    //                         </div>
    //                     </div>
    //                 </div>
    //             )
    //         })}


    //     </div>


    // )
    return (
        <div className='row mt-3   me-2 ms-2 mt-4 'style={{backgroundColor:'white'}}>
            <div className='col-11 m-auto pt-3 d-flex flex-row justify-content-between'>
                <div>
                <span className='text-dark' style={{ fontSize: '20px', fontWeight: 'bold', fontSize:'2vw' }}>Home & Kitchen Essentials</span>
                </div>
                <div>
                <button className=' btn btn-primary btn-sm ' style={{ fontSize: '20px', fontWeight: 'bold', fontSize:'2vw'}}>VIEW ALL</button>
                </div>
            </div>

            <hr className='mt-3' />

            {homeKitchenSectionItemsArray.map((eachProduct) => {
                return (
                    <div className='col-4 col-md-3 col-lg-2  m-auto  '>
                        <div className='card col-10' style={{borderWidth:'0px', marginTop:'0px', fontSize:'2vw'}}> 
                            <img src={eachProduct.image} className="card-img-top p-2 p-md-3 mx-auto image-fluid border border-success" alt="product-image " style={{ borderRightWidth: '0px', borderRadius: '0px', width: '10vw', height: '10vw' }}/>
                            <div className="card-body card-body-sm  p-0 m-2" >
                                <h5 className="card-title text-center" style={{borderWidth:'0px', marginTop:'0px', fontSize:'1vw'}}>{eachProduct.category}</h5>
                                <p className="card-text text-center text-success "  style={{borderWidth:'0px', marginTop:'0px', fontSize:'1vw'}}>From {eachProduct.companey}</p>
                                {/* <p className="card-text text-center">{eachProduct.companey}</p> */}
                            </div>
                        </div>
                    </div>
                )
            })}


        </div>
    )
}

export default HomeKitchenSection